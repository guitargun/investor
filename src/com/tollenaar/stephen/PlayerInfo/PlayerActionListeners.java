package com.tollenaar.stephen.PlayerInfo;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.tollenaar.stephen.Exeptions.AssetMaking;
import com.tollenaar.stephen.Utils.InvestorMain;

public class PlayerActionListeners implements Listener {

	private InvestorMain plugin;

	public PlayerActionListeners(InvestorMain instance) {
		this.plugin = instance;
	}

	@EventHandler
	public void onPlayerJoind(PlayerJoinEvent event) {
		PlayerAssests asset;
		try {
			asset = PlayerAssests.PlayerCheck(event.getPlayer().getUniqueId());
		} catch (AssetMaking ex) {
			Player player = event.getPlayer();
			asset = new PlayerAssests(player.getUniqueId());
			try {
				Set<ItemStack> items = plugin.file.loadPlayer(player
						.getUniqueId());
				for (ItemStack item : items) {
					asset.addItem(item);
				}
				player.sendMessage(ChatColor.GRAY + "[" + ChatColor.RED
						+ "Investor" + ChatColor.GRAY + "]" + ChatColor.RED
						+ " Welcome back. Your assests have been loaded.");
			} catch (AssetMaking e) {
				player.sendMessage(ChatColor.GRAY
						+ "["
						+ ChatColor.RED
						+ "Investor"
						+ ChatColor.GRAY
						+ "]"
						+ ChatColor.RED
						+ " Welcome. A new Investor account has been created for you.");
			}
		}
	}
	
	
	
	@EventHandler
	public void onPlayerInventory(InventoryClickEvent event){
		if(event.getClickedInventory() != null){
			if(event.getClickedInventory().getName().contains("assets")){
				event.setCancelled(true);
			}else if(event.getClickedInventory().getName().contains("shop")){
				if(event.getAction() == InventoryAction.PICKUP_ALL && event.getRawSlot() == 2 && event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR){
				Player player = (Player) event.getWhoClicked();
				PlayerAssests asset = PlayerAssests.GetPlayer(player.getUniqueId());
					asset.addItem(event.getCurrentItem());
					
					Inventory inv = event.getClickedInventory();
					inv.setItem(0, new ItemStack(Material.AIR));
					inv.setItem(1, new ItemStack(Material.AIR));
					event.setCancelled(true);
				}
			}
		}
	}
}
