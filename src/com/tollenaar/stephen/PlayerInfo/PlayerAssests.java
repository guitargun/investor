package com.tollenaar.stephen.PlayerInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.tollenaar.stephen.Exeptions.AssetMaking;
import com.tollenaar.stephen.Stock.StockItem;
import com.tollenaar.stephen.Utils.InvestorMain;

public class PlayerAssests {
	
	private static HashMap<UUID, PlayerAssests> playerass = new HashMap<UUID, PlayerAssests>();
	
	private HashSet<ItemStack> items;
	private UUID playeruuid;
	
	public PlayerAssests(UUID uuid){
		this.playeruuid = uuid;
		
		items = new HashSet<ItemStack>();
		playerass.put(uuid, this);
	}
	
	public Set<ItemStack> GetAssests(){
		return items;
	}
	
	public void addItem(ItemStack item){
		items.add(item);
	}
	
	public UUID GetUUID(){
		return playeruuid;
	}
	
	public void AssestList(Player player){
		int size = items.size();
		if(size == 0){
			size = 9;
			
			Inventory inv = Bukkit.createInventory(null, size, player.getName() + "'s assets");
			player.openInventory(inv);
			return;
			
		}
		while(size % 9 != 0){
			size++;
		}
		Inventory inv = Bukkit.createInventory(null, size, player.getName() + "'s assets");
		
		for(ItemStack item : items){
			ArrayList<String> lore = new ArrayList<String>();
			StockItem stock = StockItem.getStock(item);
			Date last = null;
			for(Date in : stock.getDates()){
				last = in;
			}
			lore.add("Value: " + stock.getValue(last));
			item.getItemMeta().setLore(lore);
			inv.addItem(item);
		}
		player.openInventory(inv);
	}
	
	
	public static PlayerAssests PlayerCheck(UUID playeruuid) throws AssetMaking{
		if(playerass.get(playeruuid) != null){
			return playerass.get(playeruuid);
		}else{
			throw new AssetMaking();
		}
	}
	
	public static PlayerAssests GetPlayer(UUID playeruuid){
		if(playerass.get(playeruuid) != null){
			return playerass.get(playeruuid);
		}else{
			return null;
		}
	}
	
	public static Set<UUID> GetAll(){
		return playerass.keySet();
	}
	
}
