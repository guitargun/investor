package com.tollenaar.stephen.Merchant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.PacketDataSerializer;
import net.minecraft.server.v1_7_R4.PacketPlayInSetCreativeSlot;
import net.minecraft.server.v1_7_R4.PacketPlayOutOpenWindow;
import net.minecraft.util.io.netty.buffer.ByteBuf;

import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import com.tollenaar.stephen.Utils.InvestorMain;

public class TradeAdding implements Listener {

	
	
	private final ArrayList<Integer> allowedslots = new ArrayList<Integer>(Arrays.asList(0, 2, 4, 8));
	
	
	private InvestorMain plugin;
	private HashSet<UUID> inadding = new HashSet<UUID>();

	public TradeAdding(InvestorMain instance) {
		this.plugin = instance;
	}

	public void AddingMenu(Player player, Villager vill) {
		player.setGameMode(GameMode.CREATIVE);
		Inventory inv = player.getInventory();
		
		
		ItemStack commit = new ItemStack(new Wool(DyeColor.GREEN).toItemStack());
		{
			ItemMeta meta = commit.getItemMeta();
			meta.setDisplayName("Commit");
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("Add trade to list");
			meta.setLore(lore);
			commit.setItemMeta(meta);
		}

		ItemStack info = new ItemStack(Material.BEDROCK);
		{
			ItemMeta meta = info.getItemMeta();
			meta.setDisplayName("info");
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(vill.getMetadata("Merchant").get(0).asString());
			meta.setLore(lore);
			info.setItemMeta(meta);
		}

		inv.setItem(8, commit);
		inv.setItem(7, info);
//		PacketPlayOutOpenWindow packet = new PacketPlayOutOpenWindow(69, 0, "Creative", 9, true);
		
		PacketPlayInSetCreativeSlot packet = new PacketPlayInSetCreativeSlot();
		EntityPlayer p = ((CraftPlayer) player).getHandle();
		p.playerConnection.sendPacket(packet);
		
		
		
		
		inadding.add(player.getUniqueId());
	}


	
	
	@EventHandler
	public void onClick(InventoryClickEvent event) {

		if (event.getClickedInventory() != null
				&& inadding.contains(((Player) event.getWhoClicked())
						.getUniqueId())) {
			

			
			if (event.getAction() == InventoryAction.PLACE_ALL
					|| event.getAction() == InventoryAction.PLACE_ONE || event.getAction() == InventoryAction.PICKUP_ALL) {

				if (allowedslots.contains(event.getSlot())) {
					Player player = (Player) event.getWhoClicked();
					if (event.getSlot() == 8) {

						Inventory in = event.getClickedInventory();

						ItemStack itemone;
						ItemStack itemtwo = null;
						ItemStack itemthree;
						if (in.getItem(0) != null
								&& in.getItem(0).getType() != Material.AIR) {
							itemone = in.getItem(0);
						} else {
							event.setCancelled(true);
							return;
						}
						if (in.getItem(2) != null
								&& in.getItem(0).getType() != Material.AIR) {
							itemtwo = in.getItem(2);
						}
						if (in.getItem(4) != null
								&& in.getItem(4).getType() != Material.AIR) {
							itemthree = in.getItem(4);
						} else {
							event.setCancelled(true);
							return;
						}

						UUID uuid = UUID.fromString(in.getItem(7).getItemMeta()
								.getLore().get(0));
						int id = plugin.person.villagertomerchant.get(uuid);
						Merchant m = plugin.itemfrabric.merchants.get(id);
						if (itemtwo != null) {
							m.addTrande(itemone, itemtwo, itemthree);
						} else {
							m.addTrande(itemone, itemthree);
						}

						
						player.sendMessage("Trade added");
						player.closeInventory();
						inadding.remove(player.getUniqueId());
						event.setCancelled(true);
					}
				} else {
					event.setCancelled(true);
				}
			}
		}
	}

}
