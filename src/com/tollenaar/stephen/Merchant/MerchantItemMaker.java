package com.tollenaar.stephen.Merchant;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.tollenaar.stephen.Utils.InvestorMain;




public class MerchantItemMaker {
	public HashMap<Integer, Merchant> merchants = new HashMap<Integer, Merchant>();
	private InvestorMain plugin;
	
	public MerchantItemMaker(InvestorMain instance){
		this.plugin =  instance;
	}
	
	
	public Merchant NewMerchant(String invname) {
		Merchant m = new Merchant(invname);
		m.addTrande(new ItemStack(Material.AIR), new ItemStack(Material.AIR));
		int i = 0;
		while (merchants.get(i) != null) {
			i++;
		}
		merchants.put(i, m);
		return m;
	}
	
	public int Newmerchant(String invname){
		Merchant m = new Merchant(invname);
		m.addTrande(new ItemStack(Material.AIR), new ItemStack(Material.AIR));
		int i = 0;
		while (merchants.get(i) != null) {
			i++;
		}
		merchants.put(i, m);
		return i;
	}
	
	
	
	public void OpenTrade(Player player, int id){
		if(merchants.get(id) != null){
			merchants.get(id).openTrande(player);
		}
	}
	
	
}
