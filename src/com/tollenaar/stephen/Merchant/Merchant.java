package com.tollenaar.stephen.Merchant;

import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.IMerchant;
import net.minecraft.server.v1_7_R4.MerchantRecipe;
import net.minecraft.server.v1_7_R4.MerchantRecipeList;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Merchant {
	private String invname;
	public MerchantRecipeList l = new MerchantRecipeList();

	public Merchant(String invname) {
		this.invname = invname = invname + "'s shop";
	}

	public Merchant addTrande(ItemStack itemone, ItemStack out) {
		if (l.size() == 1) {
			if (CraftItemStack.asBukkitCopy(
					((MerchantRecipe) l.get(0)).getBuyItem3()).getType() == Material.AIR) {
				l.remove(0);
			}
		}
		l.a(new MerchantRecipe(CraftItemStack.asNMSCopy(itemone),
				CraftItemStack.asNMSCopy(out)));

		return this;
	}

	public Merchant addTrande(ItemStack itemone, ItemStack itemtwo,
			ItemStack out) {
		if (l.size() == 1) {
			if (CraftItemStack.asBukkitCopy(
					((MerchantRecipe) l.get(0)).getBuyItem3()).getType() == Material.AIR) {
				l.remove(0);
			}
		}

		l.a(new MerchantRecipe(CraftItemStack.asNMSCopy(itemone),
				CraftItemStack.asNMSCopy(itemtwo), CraftItemStack
						.asNMSCopy(out)));
		return this;
	}

	public void openTrande(Player who) {
		final EntityHuman e = ((CraftPlayer) who).getHandle();
		e.openTrade(new IMerchant() {
			@Override
			public MerchantRecipeList getOffers(EntityHuman arg0) {
				return l;
			}

			@Override
			public EntityHuman b() {
				return e;
			}

			@Override
			public void a_(net.minecraft.server.v1_7_R4.ItemStack arg0) {
			}

			@Override
			public void a_(EntityHuman arg0) {
			}

			@Override
			public void a(MerchantRecipe arg0) {
				l.a(arg0);
			}
		}, invname);
	}

	public String getinvname() {
		return invname;
	}
}
