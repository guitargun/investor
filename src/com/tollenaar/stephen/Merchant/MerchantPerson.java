package com.tollenaar.stephen.Merchant;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.tollenaar.stephen.Utils.InvestorMain;

public class MerchantPerson implements Listener {

	private InvestorMain plugin;
	public HashMap<UUID, Integer> villagertomerchant = new HashMap<UUID, Integer>();
	private HashMap<UUID, Entity> getentity = new HashMap<UUID, Entity>();

	public MerchantPerson(InvestorMain instance) {
		this.plugin = instance;
	}

	public void CreateVillager(String name, int merchant, Location loc) {
		Villager villager = (Villager) loc.getWorld().spawnEntity(loc,
				EntityType.VILLAGER);
		UUID villageruuid = UUID.randomUUID();
		villager.setCustomName(name);
		villager.setCustomNameVisible(true);
		villager.setMetadata("Merchant", new FixedMetadataValue(plugin,
				villageruuid));

		villagertomerchant.put(villageruuid, merchant);
		getentity.put(villageruuid, villager);
	
	}

	public void CreateVillager(String name, int merchant, Location loc,
			UUID uuid) {

		Villager villager = (Villager) loc.getWorld().spawnEntity(loc,
				EntityType.VILLAGER);
		villager.setCustomName(name);
		villager.setCustomNameVisible(true);
		villager.setMetadata("Merchant", new FixedMetadataValue(plugin, uuid));
		villagertomerchant.put(uuid, merchant);
		getentity.put(uuid, villager);
	}

	public Entity ReturnEntity(UUID uuid) {
		return getentity.get(uuid);
	}

	@EventHandler
	public void onVillagerInteract(PlayerInteractEntityEvent event) {
		if (event.getRightClicked().getType() == EntityType.VILLAGER) {
			Villager vil = (Villager) event.getRightClicked();
			if (vil.hasMetadata("Merchant")) {
				Player player = event.getPlayer();

				if (player.getInventory().getItemInHand() != null
						&& player.getInventory().getItemInHand().getType() != Material.AIR
						&& player.getInventory().getItemInHand().hasItemMeta()
						&& player.getInventory().getItemInHand().getItemMeta()
								.getDisplayName().equals("trade modifier")) {
					plugin.add.AddingMenu(player, vil);
					event.setCancelled(true);
				} else {
					int id = villagertomerchant.get(UUID.fromString(vil
							.getMetadata("Merchant").get(0).asString()));
					plugin.itemfrabric.OpenTrade(player, id);
					event.setCancelled(true);
				}
			}
		}
	}

	/**
	 * anti fail listeners
	 */
	@EventHandler
	public void onVillagerDespawn(EntityDeathEvent event) {
		if (event.getEntityType() == EntityType.VILLAGER) {
			Villager death = (Villager) event.getEntity();
			if(death.hasMetadata("Merchant")){
				UUID uuid = UUID.fromString(death.getMetadata("Merchant").get(0).asString());
				int id = villagertomerchant.get(uuid);
				CreateVillager(death.getCustomName(), id, death.getLocation());
			}
		}
	}

}
