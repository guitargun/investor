package com.tollenaar.stephen.Utils;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.tollenaar.stephen.Commands.CommandMerchant;
import com.tollenaar.stephen.Commands.CommandPlayer;
import com.tollenaar.stephen.Merchant.MerchantItemMaker;
import com.tollenaar.stephen.Merchant.MerchantPerson;
import com.tollenaar.stephen.Merchant.TradeAdding;
import com.tollenaar.stephen.PlayerInfo.PlayerActionListeners;
import com.tollenaar.stephen.PlayerInfo.PlayerAssests;

public class InvestorMain extends JavaPlugin{
	
	public MerchantItemMaker itemfrabric;
	public  FileWriter file;
	public MerchantPerson person;
	public TradeAdding add;
	private PlayerActionListeners  playeraction;
	
	
	public void onEnable(){
		final FileConfiguration config = this.getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		
		itemfrabric = new MerchantItemMaker(this);
		
		person = new MerchantPerson(this);
		
		file = new FileWriter(this);
		add = new TradeAdding(this);
		
		playeraction = new PlayerActionListeners(this);
		
		getCommand("merchant").setExecutor(new CommandMerchant(this));
		getCommand("asset").setExecutor(new CommandPlayer(this));
		
		
		PluginManager pm = Bukkit.getPluginManager();
		
		pm.registerEvents(person, this);
		pm.registerEvents(add, this);
		pm.registerEvents(playeraction, this);
	}
	
	public void onDisable(){
		file.savetrade();
		for(UUID player : PlayerAssests.GetAll()){
			file.savePlayer(player);
		}
	}
}
