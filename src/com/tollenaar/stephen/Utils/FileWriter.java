package com.tollenaar.stephen.Utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.minecraft.server.v1_7_R4.MerchantRecipe;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.joptsimple.util.DateConverter;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import com.tollenaar.stephen.Exeptions.AssetMaking;
import com.tollenaar.stephen.Merchant.Merchant;
import com.tollenaar.stephen.PlayerInfo.PlayerAssests;
import com.tollenaar.stephen.Stock.StockItem;

public class FileWriter {
	private InvestorMain plugin;

	private File tradestores;

	private File playerstores;

	private File stockstore;

	public FileWriter(InvestorMain instance) {
		this.plugin = instance;

		tradestores = new File(plugin.getDataFolder(), "trades");
		if (!tradestores.exists()) {
			tradestores.mkdir();
		}

		playerstores = new File(plugin.getDataFolder(), "players");
		if (!playerstores.exists()) {
			playerstores.mkdir();
		}

		stockstore = new File(plugin.getDataFolder(), "stocks");
		if (!stockstore.exists()) {
			stockstore.mkdir();
		}

		loadtrade();
	}

	/**
	 * this is for all the playerasset stuff
	 * 
	 * @param playeruuid
	 * @throws AssetMaking
	 * 
	 */

	public Set<ItemStack> loadPlayer(UUID playeruuid) throws AssetMaking {
		HashSet<ItemStack> items = null;
		for (File file : playerstores.listFiles()) {
			if (file.getName().replace(".yml", "")
					.equals(playeruuid.toString())) {
				ConfigurationSection config = YamlConfiguration
						.loadConfiguration(file);
				items = new HashSet<ItemStack>();
				for (String k : config.getKeys(false)) {
					ItemStack item = config.getItemStack(k);
					items.add(item);
				}
			}
		}
		if (items != null) {
			return items;
		}
		throw new AssetMaking();
	}

	public void savePlayer(UUID playeruuid) {
		File file = new File(playerstores, playeruuid.toString() + ".yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		int total = 0;
		PlayerAssests asset = PlayerAssests.GetPlayer(playeruuid);
		for (ItemStack item : asset.GetAssests()) {
			config.set(Integer.toString(total), item);
			total++;
			
		}
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * this is for the trades with the villagers. needed for adding your assests
	 * in the stock market
	 */

	public void loadtrade() {
		for (File file : tradestores.listFiles()) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			String name = config.getString("name");
			int id = plugin.itemfrabric.Newmerchant(name);
			Merchant m = plugin.itemfrabric.merchants.get(id);
			if (config.getConfigurationSection("Items") != null) {
				for (String keys : config.getConfigurationSection("Items")
						.getKeys(false)) {

					ItemStack in = config.getItemStack("Items." + keys
							+ ".itemone");
					ItemStack out = config.getItemStack("Items." + keys
							+ ".itemout");

					if (config.getItemStack("Items." + keys + ".itemtwo") != null) {
						ItemStack two = config.getItemStack("Items." + keys
								+ ".itemtwo");
						m.addTrande(in, two, out);
					} else {
						m.addTrande(in, out);
					}

				}

				UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));
				Location l = new Location(Bukkit.getWorld(config
						.getString("world")), config.getDouble("x"),
						config.getDouble("y"), config.getDouble("z"));
				plugin.person.CreateVillager(name, id, l, uuid);

			}
		}
	}

	public void savetrade() {

		for (UUID uuid : plugin.person.villagertomerchant.keySet()) {
			File file = new File(tradestores, uuid.toString() + ".yml");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);

			Entity en = plugin.person.ReturnEntity(uuid);

			int id = plugin.person.villagertomerchant.get(uuid);
			Merchant m = plugin.itemfrabric.merchants.get(id);

			config.set("name", m.getinvname().replace("'s shop", ""));

			config.set("x", en.getLocation().getX());
			config.set("y", en.getLocation().getY());
			config.set("z", en.getLocation().getZ());
			config.set("world", en.getLocation().getWorld().getName());

			int total = 0;
			for (int i = 0; i < m.l.size(); i++) {

				MerchantRecipe rec = (MerchantRecipe) m.l.get(i);
				ItemStack bukkitbuy = CraftItemStack.asBukkitCopy(rec
						.getBuyItem1());
				ItemStack bukkitsell = CraftItemStack.asBukkitCopy(rec
						.getBuyItem3());

				if (bukkitsell.getType() != Material.AIR) {

					config.set("Items." + total + ".itemone", bukkitbuy);
					config.set("Items." + total + ".itemout", bukkitsell);

					if (rec.getBuyItem2() != null) {
						config.set("Items." + total + ".itemtwo",
								CraftItemStack.asBukkitCopy(rec.getBuyItem2()));
					}

					total++;
				}
			}
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}

			plugin.person.ReturnEntity(uuid).remove();
		}

	}

	/**
	 * this is for all the items in the stock market
	 * 
	 * @param item
	 *            as the stock
	 * @param value
	 *            from the stock
	 */

	public void savestock(ItemStack item, HashMap<Date, Integer> value) {
		File file = new File(stockstore, item.getTypeId() + ":"
				+ item.getData().getData() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		config.set("Item", item);
		for (Date date : value.keySet()) {
			config.set(
					date.getDay() + "/" + date.getMonth() + "/"
							+ date.getYear(), value.get(date));
		}

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadstock() {
		for (File file : stockstore.listFiles()) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			ItemStack item = config.getItemStack("Item");
			HashMap<Date, Integer> values = new HashMap<Date, Integer>();
			for (String keys : config.getKeys(false)) {
				if (keys.contains("/")) {
					String[] split = keys.split("/");
					int year = Integer.parseInt(split[2]);
					int month = Integer.parseInt(split[1]);
					int day = Integer.parseInt(split[0]);
					Date date = new Date(year, month, day);
					int v = config.getInt(keys);
					values.put(date, v);
				}
			}
			new StockItem(item, values);

		}

	}

}
