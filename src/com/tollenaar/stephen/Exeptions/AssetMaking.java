package com.tollenaar.stephen.Exeptions;

public class AssetMaking extends Exception{
	
	private static final long serialVersionUID = -748739792546888820L;

	public AssetMaking(){
	}

	public AssetMaking(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public AssetMaking(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public AssetMaking(String arg0) {
		super(arg0);
	}

	public AssetMaking(Throwable arg0) {
		super(arg0);
	}
	
	
}
