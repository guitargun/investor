package com.tollenaar.stephen.Stock;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.inventory.ItemStack;

public class StockItem {

	private static HashMap<ItemStack, StockItem> allstock = new HashMap<ItemStack, StockItem>();
	
	private ItemStack item;
	private HashMap<Date, Integer> value;

	public StockItem(ItemStack item, Integer v) {
		this.item = item;
		value = new HashMap<Date, Integer>();
		value.put(new Date(System.currentTimeMillis()), v);
		allstock.put(item, this);
	}

	public StockItem(ItemStack item, HashMap<Date, Integer> value) {
		this.item = item;
		this.value = value;
		allstock.put(item, this);
	}

	public ItemStack getItem() {
		return item;
	}

	public Integer getValue(Date date) {
		if (value.get(date) != null) {
			return value.get(date);
		} else {
			return 0;
		}
	}

	public Set<Date> getDates() {
		return value.keySet();
	}
	
	
	public static StockItem getStock(ItemStack item){
		if(allstock.get(item) != null){
			return allstock.get(item);
		}else{
			return new StockItem(item, 0);
		}
	}
	

}
