package com.tollenaar.stephen.Stock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.bukkit.inventory.ItemStack;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.tollenaar.stephen.Utils.InvestorMain;

public class Graph{
	private InvestorMain plugin;
	
	
	
	
	public Graph(InvestorMain instance){
		this.plugin = instance;
	}
	
	public void MakeHolo(Hologram holo, ItemStack item){
		StockItem stock = StockItem.getStock(item);
		String v ="";
		
		int prev = -999;
		for(Date d : stock.getDates()){
			int val = stock.getValue(d);
			if(prev == -999){
				prev = val;
				v += "_";
			}else if(prev == val){
				v += "_";
			}else if(prev < val){
				v += "\\";
			}else{
				v += "/";
			}
		}
		for(String in : MakeGraph(v)){
			holo.appendTextLine(in);
		}
	}
	
	
	private ArrayList<String> MakeGraph(String a){
		HashMap<Integer, String> s = new HashMap<Integer, String>();
		int lvl = 0;
		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == '/') {
				String k = s.get(lvl);
				k += a.charAt(i);
				s.put(lvl, k);
				lvl++;
				String insert;
				if (s.get(lvl) == null) {
					insert = " ";
					for (int x = 0; x < i; x++) {
						insert += " ";
					}
					s.put(lvl, insert);
				} else {
					insert = s.get(lvl);
					for (int x = insert.length() - 1; x < i; x++) {
						insert += " ";
					}
					s.put(lvl, insert);
				}
			} else if (a.charAt(i) == 'v') {
				lvl--;
				if (lvl < 0) {
					for (int x = s.size()-1; x >= 0; x--) {
						s.put(x + 1, s.get(x));
					}
					s.remove(0);
					lvl = 0;
				}
				String insert;
				if (s.get(lvl) == null) {
					insert = "";
					for (int x = 0; x < i; x++) {
						insert += " ";
					}
					insert += "\\";
					s.put(lvl, insert);
				} else {
					insert = s.get(lvl);
					for (int x = insert.length(); x < i; x++) {
						insert += " ";
					}
					insert += "\\";
					s.put(lvl, insert);
				}

			} else {
				if (s.get(lvl) == null) {
					String insert = Character.toString(a.charAt(i));
					s.put(lvl, insert);
				} else {
					String k = s.get(lvl);
					k += Character.toString(a.charAt(i));
					s.put(lvl, k);
				}
			}

		}
		
		ArrayList<String> info = new ArrayList<String>();
		for (int i = s.size() - 1; i >= 0; i--) {
			info.add(s.get(i));
		}
		return info;
	}
}
