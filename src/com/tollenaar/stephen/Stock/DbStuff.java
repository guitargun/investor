package com.tollenaar.stephen.Stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ScheduledFuture;

import com.tollenaar.stephen.Utils.InvestorMain;
import com.tollenaar.stephen.Utils.Scheduler;

import code.husky.mysql.MySQL;

public class DbStuff {
	protected Connection con;
	public MySQL MySQl;
	private String mysqlpass;
	private String mysqluser;
	private String mysqldb;
	private String mysqlpot;
	private String mysqlhost;
	private ScheduledFuture<?> scheduler;
	private InvestorMain plugin;

	public DbStuff(InvestorMain instance) {
		this.plugin = instance;
	}

	public void closecon() {
		int timeout = 10;

		ScheduledFuture<?> id = Scheduler.runAsyncTaskRepeat(new Runnable() {
			public void run() {
				PreparedStatement pst;
				try {
					pst = con
							.prepareStatement("SELECT playeruuid FROM `investor_player` LIMIT 1;");
					pst.execute();
				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
				}
			}
		}, 0L, timeout);
		scheduler = id;
	}

	public void opencon() {
		scheduler.cancel(true);
	}

	public void intvar() {
		mysqlpass = plugin.getConfig().getString("mysqlpass");
		mysqluser = plugin.getConfig().getString("mysqluser");
		mysqldb = plugin.getConfig().getString("mysqldb");
		mysqlpot = plugin.getConfig().getString("mysqlport");
		mysqlhost = plugin.getConfig().getString("mysqlhost");
		MySQl = new MySQL(plugin, mysqlhost, mysqlpot, mysqldb, mysqluser,
				mysqlpass);
	}
	
	
	
	
}
