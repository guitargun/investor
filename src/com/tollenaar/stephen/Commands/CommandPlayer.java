package com.tollenaar.stephen.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.tollenaar.stephen.PlayerInfo.PlayerAssests;
import com.tollenaar.stephen.Utils.InvestorMain;

public class CommandPlayer implements CommandExecutor{
	private InvestorMain plugin;
	
	public CommandPlayer(InvestorMain instance){
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandlabel,
			String[] args) {
		if(sender instanceof Player){
			Player player = (Player) sender;
			String command = cmd.getName();
			
			if(command.equalsIgnoreCase("asset")){
				PlayerAssests.GetPlayer(player.getUniqueId()).AssestList(player);
				return true;
			}
			
			
		}
		
		
		return true;
	}
	
	
	
	
}
