package com.tollenaar.stephen.Commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.tollenaar.stephen.Utils.InvestorMain;

public class CommandMerchant implements CommandExecutor {

	private InvestorMain plugin;

	public CommandMerchant(InvestorMain instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandlabel, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (args[0].equalsIgnoreCase("create")) {
				if (args.length == 2) {
					int id = plugin.itemfrabric.Newmerchant(args[1]);
					plugin.person.CreateVillager(args[1],
							id,
							player.getLocation());
					return true;
				}
			} else if (args[0].equalsIgnoreCase("open")) {
				if (args.length > 0) {
					int id = Integer.parseInt(args[1]);
					plugin.itemfrabric.merchants.get(id).openTrande(player);
					return true;
				}
			}else if(args[0].equalsIgnoreCase("tool")){
				ItemStack tool = new ItemStack(Material.DIAMOND_HOE);
				{
					ItemMeta meta = tool.getItemMeta();
					meta.setDisplayName("trade modifier");
					tool.setItemMeta(meta);
				}
				player.getInventory().setItemInHand(tool);
				return true;
			}

		}
		sender.sendMessage("Unknown Command. Try using /merchant <create/open/tool>");
		return true;
	}
}
